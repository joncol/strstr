pub fn find_string(haystack: String, needle: String) -> i32 {
    for i in 0..haystack.len() - (needle.len() - 1) {
        let mut found = true;
        for j in 0..needle.len() {
            if haystack.chars().nth(i + j) != needle.chars().nth(j) {
                found = false;
                break;
            }
        }
        if found {
            return i as i32;
        }
    }
    -1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(
            find_string("abracadabra".to_string(), "abr".to_string()),
            0
        );
    }

    #[test]
    fn test2() {
        assert_eq!(
            find_string("abracadabra".to_string(), "daba".to_string()),
            -1
        );
    }

    #[test]
    fn test3() {
        assert_eq!(
            find_string("abracadabra".to_string(), "acad".to_string()),
            3
        );
    }

    #[test]
    fn test4() {
        let prefix_len = 10e3 as i32;
        let mut long_haystack =  (0..prefix_len).map(|_| "x").collect::<String>();
        long_haystack.push_str("abc");
        assert_eq!(
            find_string(long_haystack, "abc".to_string()),
            prefix_len
        );
    }
}
