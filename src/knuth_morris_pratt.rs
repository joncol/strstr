pub fn find_string(haystack: String, needle: String) -> i32 {
    let t = haystack.as_bytes();
    let p = needle.as_bytes();
    let n = t.len();
    let m = p.len();
    let prefix_fun = compute_prefix_function(&needle);
    let mut q: usize = 0;
    for i in 0..n {
        while q > 0 && p[q] != t[i] {
            q = prefix_fun[q - 1];
        }
        if p[q] == t[i] {
            q += 1;
        }
        if q == m {
            return (i + 1 - m) as i32;
        }
    }
    -1
}

fn compute_prefix_function(pattern: &String) -> Vec<usize> {
    let p = pattern.as_bytes();
    let m = p.len();
    let mut prefix_fun = Vec::with_capacity(m);
    prefix_fun.resize(m, 0);
    prefix_fun[1] = 0;
    let mut k: usize = 0;
    for q in 1..m {
        while k > 0 && p[k] != p[q] {
            k = prefix_fun[k - 1];
        }
        if p[k] == p[q] {
            k += 1;
        }
        prefix_fun[q] = k;
    }
    prefix_fun
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_prefix_function1() {
        assert_eq!(
            compute_prefix_function(&"ababababca".to_string()),
            vec![0, 0, 1, 2, 3, 4, 5, 6, 0, 1]
        );
    }

    #[test]
    fn test_compute_prefix_function2() {
        assert_eq!(compute_prefix_function(&"bba".to_string()), vec![0, 1, 0]);
    }

    #[test]
    fn test1() {
        assert_eq!(find_string("abracadabra".to_string(), "abr".to_string()), 0);
    }

    #[test]
    fn test2() {
        assert_eq!(
            find_string("abracadabra".to_string(), "daba".to_string()),
            -1
        );
    }

    #[test]
    fn test3() {
        assert_eq!(
            find_string("abracadabra".to_string(), "acad".to_string()),
            3
        );
    }

    #[test]
    fn test4() {
        let prefix_len = 10e3 as i32;
        let mut long_haystack = (0..prefix_len).map(|_| "x").collect::<String>();
        long_haystack.push_str("abc");
        assert_eq!(find_string(long_haystack, "abc".to_string()), prefix_len);
    }

    #[test]
    fn test5() {
        assert_eq!(
            find_string("mississippi".to_string(), "issip".to_string()),
            4
        );
    }
}
