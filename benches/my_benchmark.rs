use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use strstr::naive_search;
use strstr::knuth_morris_pratt;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Strstr");
    for prefix_len in [10e2 as i32, 10e3 as i32, 50e3 as i32].iter() {
        if *prefix_len == 50e3 as i32 {
            group.sample_size(40);
        }
        group.bench_with_input(
            BenchmarkId::new("Naive search", prefix_len),
            prefix_len,
            |b, prefix_len| {
                b.iter(|| {
                    naive_search::find_string(
                        black_box(long_haystack(*prefix_len)),
                        black_box("abc".to_string()),
                    )
                })
            },
        );
        // Restore default sample size for KMP.
        if *prefix_len == 50e3 as i32 {
            group.sample_size(100);
        }
        group.bench_with_input(
            BenchmarkId::new("Knuth-Morris-Pratt", prefix_len),
            prefix_len,
            |b, prefix_len| {
                b.iter(|| {
                    knuth_morris_pratt::find_string(
                        black_box(long_haystack(*prefix_len)),
                        black_box("abc".to_string()),
                    )
                })
            },
        );
    }
    group.finish();
}

fn long_haystack(n: i32) -> String {
    let mut t = (0..n).map(|_| "a").collect::<String>();
    t.push_str("b");
    t
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
